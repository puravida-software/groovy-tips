= Groovy intro: Tips para QAs
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2019-04-01
:revnumber: {project-version}
:example-caption!:
:revealjs_transition: linear
:revealjs_history: true
:revealjs_slideNumber: true
ifndef::imagesdir[:imagesdir: images]
ifndef::sourcedir[:sourcedir: ../java]

== Objetivo

* Introducción a Groovy, orientado a QA
* No es un curso
* Conceptos tipos básicos, listas, mapas
* Json, XML y CSV
* Scripts vs Project
* Librerías utiles QA

== PuraVida Software

.Qué hacemos
barcode::qrcode[https://puravida-software.gitlab.io//#/,300,300]

== Controles

NOTE: Si no te gusta el color de la presentación, o no se ve bien
en tu monitor, utiliza el menú inferior izquierda para cambiar de
tema. También puedes jugar con el efecto al cambiar transparencias
si es que te parece muy sosa.


== Tipos básicos

* Qué añade Groovy a los típicos
* String
* Date
* def

== String (GString)

* comillas simples, dobles y triples
* interpolación

== Listas

* [ 1, 2, "hola", 3.4, new Date() ]
* .reverse()
* .find .findAll .each .eachWithIndex

== Mapas

* [ a : 1, b: "hola" ]
* .find .findAll .each -eachWithIndex
* a.b = c
* key and value

== JSON

* mapa parece, json es
* de json a mapa y viceversa
* pintar un json para humanos

== XML

* leer y parsear xml made easy
* json o xml

== CSV

* leer un fichero 
* parsear un csv

== Scripts

* script vs project
* potencia del script
* necesidad del project

== Librerías

* http y/o SOAP
* Spock y/o JUnit
* Geb (y Selenium)
* ...

== Extra balls

* Gradle
* Git







